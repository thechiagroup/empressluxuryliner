<?php include 'includes/doctype.html'; ?>  

<title>Winnipeg's Empress Luxury Liner: a rental and charter luxury bus. Special event transportation for weddings, executive meeting transport, management retreats, entertainment and music groups, golf events. Finest coach service in Manitoba. Delivering luxury.</title>
	
<?php include 'includes/header.html'; ?>
            <div id="main_nav">
                            
                <div id="usual1" class="usual"> 
                      <ul> 
                        <li><a href="features.php">Features</a></li> 

                        <li><a href="contact.php">Contact Us</a></li> 
                        <li><a href="pricing.php">Pricing</a></li>
                        <li> </li>
                      </ul> 
                    </div>  <!-- end  id="usual1" class="usual" -->

                     <div id="sub_nav_new">
                        </ul> 
                     </div> <!-- end id=main_nav -->
          </div> <!-- end id=header -->

        <body id="body_index">
        
        <div id="content_area" class="content-home">

<!-- Image list for the rotating banner front page. -->
				<img src="images/front01.jpg" alt="" class="first" />
				<img src="images/front03.jpg" alt="" />
				<img src="images/front04.jpg" alt="" /> 
  				<img src="images/front05.jpg" alt="" />
                <img src="images/front06.jpg" alt="" />

                
		 <div id="text_box">

			<h1 style="text-align:center;">The Empress delivers elegant people in an elegant manner.</h1>
           <p align="center">Whether taking your top performers out for a congratulatory dinner, transporting your wedding party from ceremony to photo session to celebration, or enjoying a night out for that special occasion, the Empress Luxury Liner will provide you and your guests with a comfortable, safe, and entertaining ride - complete with private driver delivering the utmost in hospitality. Serving Winnipeg, Manitoba and destinations much further away, our services are a perfect alternative to a Limousine or full size Coach.</p>
         </div>  <!-- end id=text_box -->


        </div>  <!-- end id=content_area -->
<?php include 'includes/footer.html'; ?>	

</div>

</body>
</html>
