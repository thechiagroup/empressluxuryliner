<?php include 'includes/doctype.html'; ?>  

<title>Careers: Winnipeg's Empress Luxury Liner: a rental and charter luxury bus. Special event transportation for weddings, executive meeting transport, management retreats, entertainment and music groups, golf events. Finest coach service in Manitoba.</title>
<?php include 'includes/header.html'; ?>

            <div id="main_nav">
                            
                <div id="usual1" class="usual"> 
                                            <ul> 
                        <li><a href="features.asp">Features</a></li> 
                        <li><a href="contact.asp">Contact Us</a></li> 
                        <li><a href="pricing.asp">Pricing</a></li>
                        <li> </li>
                      </ul> 
                     </div> <!-- end id=main_nav -->
          </div> <!-- end id=header -->
        
        <body id="body_careers">
        
        <div id="content_area_sub">
       	  <div id="main_image"></div>
		  <div id="text_box_single">
            <table cellspacing="9">
              <tr><td width="1044"><h1>Careers</h1>			  
			  <p>We are always seeking qualified drivers for the Empress Luxury Liner. If you are passionate about people, have the necessary licencing and experience,  and impeccable service standards we’d love to hear from you. We are an equal  opportunity employer and therefore invite and encourage all to apply.</p>			  
			  <p>Please e-mail your cover letter and resume to <a href="mailto:info@empressluxuryliner.ca">info@empressluxuryliner.ca</a> for us to  reference as job opportunities become available.</p>
			  <p><br>
			    </p></td>              
                            </table>
		  </div> <!-- end id=text_box -->
        </div> 
        <!-- end id=content_area -->
		
<?php include 'includes/footer.html'; ?>	

</body>
</html>
