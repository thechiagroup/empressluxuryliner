<?php include 'includes/doctype.html'; ?>	

<title>Winnipeg's Empress Luxury Liner: a rental and charter luxury bus liner. Special event transportation for weddings, executive meeting transport, management retreats, entertainment and music groups, golf events. Finest coach service in Manitoba.</title>
<?php include 'includes/header.html'; ?>	

            <div id="main_nav">
                            
                <div id="usual1" class="usual"> 
                                            <ul> 
                        <li><a href="features.php">Features</a></li> 

                        <li><a href="contact.php">Contact Us</a></li> 
                        <li><a href="pricing.php">Pricing</a></li>
                        <li> </li>
                      </ul> 
                    </div>  <!-- end  id="usual1" class="usual" -->

                     <div id="sub_nav_new">
                          <ul> 
                            <li><a href="careers.php">Careers</a></li> 
                        </ul> 
                     </div> <!-- end id=main_nav -->
          </div> <!-- end id=header -->
        
        <body id="body_our_people">
        
        <div id="content_area_sub">
       	  <div id="main_image"></div>
		  <div id="text_box_single">
            <table cellspacing="9">
              <tr><td width="1044"><h1>Our People</h1>			  
			  <p>The Empress  prides itself on having some of the most experienced staff driving on the road. All members of our staff are trained extensively in customer service as well as having years of driving experience with large passenger vehicles.<br>
			  </p></td>              
                            </table>
		  </div> <!-- end id=text_box -->
        </div> 
        <!-- end id=content_area -->
		
<?php include 'includes/footer.html'; ?>	
</body>
</html>
