var fade_speed = 350;
var home_page_rotation_duration = 2200,home_page_rotation_speed = 8200;

<!--home_page_rotation_duration is the transition speed, home_page_rotation_speed is length of time on-screen for each image-->

function home_image_slide()
{
	var total_images = $("div.content-home img").length;
	var current_image = -1,next_image = -1;

	$("div.content-home img").each(function(i) {
		if ($(this).css("left") == 0 || $(this).css("left") == "0px")
		{
			current_image = i;
			next_image = current_image + 1 < total_images ? current_image + 1 : 0;
		}
	});

	$("div.content-home img:not(:eq(" + current_image + "))").css("left","-1000px");

	$("div.content-home img:eq(" + current_image + ")").animate({"left":"1000px"},home_page_rotation_duration);
	$("div.content-home img:eq(" + next_image + ")").animate({"left":"0px"},home_page_rotation_duration);
}

$(document).ready(function() {
	$("ul#main-navigation a,ul#sub-navigation a,ul#bottom-navigation a")
		.each(function() {
			var anchor_text = $(this).text();
			$(this).text("");
			$(document.createElement("span"))
				.text(anchor_text)
				.css({opacity:0,display:"block"})
				.appendTo($(this));
		})
		.hover(function() {
			$(this).find("span").stop().fadeTo(fade_speed,1);
		},function() {
			$(this).find("span").stop().fadeTo(fade_speed,0);
		});

	if ($("div.content-home img").length > 1)
	{
		setInterval("home_image_slide()",home_page_rotation_speed);
	}
});