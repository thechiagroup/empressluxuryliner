<?php include 'includes/doctype.html'; ?>	

<title>Privacy Policy: Winnipeg's Empress Luxury Liner: a rental and charter luxury bus liner. Special event transportation for weddings, executive meeting transport, management retreats, entertainment and music groups, golf events. Finest coach service in Manitoba.</title>
	
<?php include 'includes/header.html'; ?>	

            <div id="main_nav">
                            
                <div id="usual1" class="usual"> 
                                            <ul> 
                        <li><a href="features.php">Features</a></li> 

                        <li><a href="contact.php">Contact Us</a></li> 
                        <li><a href="pricing.php">Pricing</a></li>
                        <li> </li>
                      </ul> 
                    </div>  <!-- end  id="usual1" class="usual" -->

                     <div id="sub_nav_new">
                          <ul> 
                            <li> </li> 
                        </ul> 
                     </div> <!-- end id=main_nav -->
          </div> <!-- end id=header -->
        
        <body id="body_privacy">
        
        <div id="content_area_sub">
        	<div id="main_image"><img src="images/sub_image.jpg" alt="Empress Luxury Liner" /></div>
	    <div id="text_box_single">
			  <h1>Our Privacy Promise to You</h1>
                              
                
<p>The Staff and Management of the Empress Luxury Liner are committed to protecting your privacy and maintaining a strong bond of trust and descretion with our customers. This Privacy Policy explains the use of information collected via our websites and our customer service operations (collectively, "Salisbury Cruise Lines" or the "Empress Luxury Liner") that we operate. Your use of our services indicates to us that you have read and accept our privacy practices, as outlined in this Privacy Policy. Except under certain non-marketing related limited circumstances, staff and management of the Empress Luxury Liner does not disclose to third parties, the personal information or other account-related information you provide us, without your permission.</p>

<p>We may disclose your personal or other account-related information under the following circumstances:</p>

 

<ul>
<li>If we believe in good faith that such disclosure is necessary under applicable law, or to comply with legal process served on Empress Luxury Liner;</li>
<li>In order to protect and defend the rights or interests of Empress Luxury Liner;</li>
<li>In order to report to law enforcement authorities, or assist in their investigation of, suspected illegal or wrongful activity, or to report any instance in which we believe a person may be in danger;</li>
<li>To service providers with whom we have contracted to assist us with Empress Luxury Liner features or operations (such as government bodies), and to fulfill your service requests. Our contracts with these third parties will prohibit them from using any of your personal information for purposes unrelated to the product or service they are providing;</li>
<li>To other third parties (a) to provide you with services you have requested, (b) to offer you information about our Service (e.g., events or features), or (c) to whom you explicitly ask us to send your information (or about whom you are otherwise explicitly notified and consent to when using a specific service). For instance, we may provide certain information to our payment processor, to credit card associations, banks or issuers (if you are using a credit card), or to providers of other services you request. If you choose to use these third parties' products or services, then their use of your information is governed by their privacy policies. You should evaluate the practices of third party providers before deciding to use their services;
and</li>
<li>To other business entities, should we plan to merge with or be acquired by that business entity. Should such a combination occur, we will require that the new combined entity follow this Privacy Policy with respect to your personal information. If your personal information will be used contrary to this policy, you will receive prior notice.</li>
</ul>

<h2>Audio/Visual Privacy Policy</h2>

<p>Empress Luxury Liner staff may take photos, audio recordings or videos of clients at their request. We will not take photos, audio recordings or videos of our clients unless requested by the client to do so. Use of this material for the purpose of marketing the Empress Luxury Liner will only be done with the agreement of the client prior to its use.</p>                 
                  
                    




            </div> <!-- end id=text_box -->
        </div> 
        <!-- end id=content_area -->
		
<?php include 'includes/footer.html'; ?>	

</body>
</html>
