<?php include 'includes/doctype.html'; ?>  

<title>Features of Winnipeg's Empress Luxury Liner: a rental and charter luxury bus. Special event transportation for weddings, executive meeting transport, management retreats, entertainment and music groups, golf events. Finest coach service in Manitoba with the finest interior detailing.</title>

<style type="text/css" media="screen">
		p, table, hr, .box {
}
		.box p { margin-bottom:10px; }
		span.tooltip_green, span.tooltip_yellow, span.tooltip_orange, span.tooltip_red, span.tooltip_pink, span.tooltip_darkblue, span.tooltip_lightblue, span.tooltip_white { font-weight: bold; }
	body {
	background-color: #000;
}
    </style>
    <?php include 'includes/header.html'; ?>
	

            <div id="main_nav">
                            
                <div id="usual1" class="usual"> 
                      <ul> 
                        <li><a href="features.php">Features</a></li> 

                        <li><a href="contact.php">Contact Us</a></li> 
                        <li><a href="pricing.php">Pricing</a></li>
                        <li> </li>
                      </ul> 
                     </div> <!-- end id=main_nav -->
          </div> <!-- end id=header -->
        
        <body id="body_features">
        
        <div id="content_area_sub">
        	<div id="main_image"><img src="images/interior_top.jpg" alt="Interior of Empress Luxury Liner" width="1000" height="225" /></div>
         
         
         <div>
            <table width="998" height="338" border="0" align="center" cellpadding="0" cellspacing="0" id="Table_01">
	<tr>
		<td rowspan="11">
			<img src="images/EmpressDiagramBus_01.png" width="147" height="338" alt=""></td>
		<td rowspan="8">
			<img src="images/EmpressDiagramBus_02.png" width="44" height="233" alt=""></td>
		<td rowspan="11">
			<img src="images/EmpressDiagramBus_03.gif" width="93" height="338" alt=""></td>
		<td rowspan="8">
			<img src="images/EmpressDiagramBus_04.png" width="44" height="233" alt=""></td>
		<td rowspan="11">
			<img src="images/EmpressDiagramBus_05.png" width="77" height="338" alt=""></td>
		<td rowspan="3">
			<img src="images/EmpressDiagramBus_06.png" width="47" height="115" alt=""></td>
		<td rowspan="11">
			<img src="images/EmpressDiagramBus_07.png" width="43" height="338" alt=""></td>
		<td>
			<img src="images/EmpressDiagramBus_08.png" width="40" height="62" alt=""></td>
		<td rowspan="11">
			<img src="images/EmpressDiagramBus_09.png" width="88" height="338" alt=""></td>
		<td rowspan="3">
			<img src="images/EmpressDiagramBus_10.png" width="46" height="115" alt=""></td>
		<td rowspan="11">
			<img src="images/EmpressDiagramBus_11.png" width="54" height="338" alt=""></td>
		<td rowspan="2">
			<img src="images/EmpressDiagramBus_12.png" width="47" height="83" alt=""></td>
		<td rowspan="11">
			<img src="images/EmpressDiagramBus_13.png" width="36" height="338" alt=""></td>
		<td rowspan="6">
			<img src="images/EmpressDiagramBus_14.png" width="43" height="169" alt=""></td>
		<td rowspan="11">
			<img src="images/EmpressDiagramBus_15.png" width="149" height="338" alt=""></td>
		<td>
			<img src="images/spacer.gif" width="1" height="62" alt=""></td>
	</tr>
	<tr>
		<td rowspan="2">
			<img src="images/EmpressDiagramBus_16.png" 
            class="tooltip_white" text="<strong>Sound System and DVD player</strong>multiple inputs available for any portable sound player and DVD player" width="40" height="53" alt=""></td>
		<td>
			<img src="images/spacer.gif" width="1" height="21" alt=""></td>
	</tr>
	<tr>
		<td rowspan="2">
			<img src="images/EmpressDiagramBus_17.png" 
            class="tooltip_white" text="<strong>17 inch TV</strong>flat screen TV with multiple inputs available" width="47" height="48" alt=""></td>
		<td>
			<img src="images/spacer.gif" width="1" height="32" alt=""></td>
	</tr>
	<tr>
		<td rowspan="2">
			<img src="images/EmpressDiagramBus_18.png" class="tooltip_white" text="<strong>Leather seating</strong>High end, fine hand, semi-aniline leather on all seating." width="47" height="43" alt=""></td>
		<td rowspan="5">
			<img src="images/EmpressDiagramBus_19.png" width="40" height="118" alt=""></td>
		<td rowspan="2">
			<img src="images/EmpressDiagramBus_20.png" class="tooltip_white" text="<strong>Galley</strong>Fully stocked galley with glasses and serving ware." width="46" height="43" alt=""></td>
		<td>
			<img src="images/spacer.gif" width="1" height="16" alt=""></td>
	</tr>
	<tr>
		<td rowspan="7">
			<img src="images/EmpressDiagramBus_21.gif" width="47" height="207" alt=""></td>
		<td>
			<img src="images/spacer.gif" width="1" height="27" alt=""></td>
	</tr>
	<tr>
		<td rowspan="4">
			<img src="images/EmpressDiagramBus_22.png" width="47" height="118" alt=""></td>
		<td rowspan="3">
			<img src="images/EmpressDiagramBus_23.png" width="46" height="75" alt=""></td>
		<td>
			<img src="images/spacer.gif" width="1" height="11" alt=""></td>
	</tr>
	<tr>
		<td>
			<img src="images/EmpressDiagramBus_24.png" class="tooltip_white" text="<strong>Conference Room</strong>Seat and table with door for additional privacy." width="43" height="44" alt=""></td>
		<td>
			<img src="images/spacer.gif" width="1" height="44" alt=""></td>
	</tr>
	<tr>
		<td rowspan="4">
			<img src="images/EmpressDiagramBus_25.png" width="43" height="125" alt=""></td>
		<td>
			<img src="images/spacer.gif" width="1" height="20" alt=""></td>
	</tr>
	<tr>
		<td>

<img src="images/EmpressDiagramBus_26.png" class="tooltip_white" text="<strong>27 inch TV</strong>flat screen TV with multiple inputs available" width="44" height="43">


</td>
		<td>
			<img src="images/EmpressDiagramBus_27.png" class="tooltip_white" text="<strong>Wood Detailing</strong>hand polished wood detailing throughout the Empress Luxury Liner" width="44" height="43" alt=""></td>
		<td>
			<img src="images/EmpressDiagramBus_28.png" class="tooltip_white" text="<strong>Marble Table Top</strong>Marble food/drink work station counter top." width="40" height="43" alt=""></td>
		<td>
			<img src="images/EmpressDiagramBus_29.png" class="tooltip_white" text="<strong>Washroom</strong>Large washroom." width="46" height="43" alt=""></td>
		<td>
			<img src="images/spacer.gif" width="1" height="43" alt=""></td>
	</tr>
	<tr>
		<td rowspan="2">
			<img src="images/EmpressDiagramBus_30.gif" width="44" height="62" alt=""></td>
		<td rowspan="2">
			<img src="images/EmpressDiagramBus_31.png" width="44" height="62" alt=""></td>
		<td>
			<img src="images/EmpressDiagramBus_32.png" class="tooltip_white" text="<strong>Full size sink</strong>hot and cold running water, stove, and kitchen tools available."
            
            width="47" height="43" alt=""></td>
		<td rowspan="2">
			<img src="images/EmpressDiagramBus_33.png" width="40" height="62" alt=""></td>
		<td rowspan="2">
			<img src="images/EmpressDiagramBus_34.png" width="46" height="62" alt=""></td>
		<td>
			<img src="images/spacer.gif" width="1" height="43" alt=""></td>
	</tr>
	<tr>
		<td>
			<img src="images/EmpressDiagramBus_35.png" width="47" height="19" alt=""></td>
		<td>
			<img src="images/spacer.gif" width="1" height="19" alt=""></td>
	</tr>
</table>
</div>
<div id="text_box_single">
			  <h1>Features of the Empress</h1>
              <p> The  Empress Luxury Liner  provides guests luxury and comfort. Whether   taking your top performers out for a congratulatory dinner, transporting   your wedding party from ceremony to photo session to celebration,   or enjoying a night out for that special occasion, the Empress Luxury   Liner will provide you and your guests with a comfortable, safe and   entertaining ride - complete with private driver delivering the utmost   in hospitality. <strong><br />
              <br />
              </strong><strong>THE EMPRESS LUXURY LINER COMES EQUIPPED WITH</strong>:</p>
              <ul>
                <li>                Climate-controlled coach for up to 16 people.</li>
                <li>                A/V system with DVD/CD/MP3/Sirius capabilities. We encourage guests to bring their own music or entertainment, the Empress has access for almost all entertainment systems.<br />
                  ---SDB4.1  Russound Speaker Vol. Switch <br />
                  ---Marantz  MCR603 Amp/built in cd player-with iPod hookup. <br />
                  ---Samsung  LN22C450--LCD <br />
                  ---Samsung  LN32D4000-LCD <br />
                  ---Sony  BDPS480 Blu-Ray Player <br />
                  ---subwoofers 2pr.--Paradigm  Stylus 170 Speakers <br />
                ---Sirius  Radio docking station </li>
                <li>Conference boardroom area to conduct your mobile meetings. </li>
                <li>Flat screen TV in conference boardroom and flat screen TV behind driver.</li>
                <li>Galley with microwave, fridge/stove, and bar seating. </li>
                <li>Bathroom.</li>
                <li>Complimentary bottled water.</li>
                <li>Bar with glassware. </li>
                <li>Beautiful accents throughout, such as top-grain semi-aniline leather seating and hand-polished wood detailing.</li>
              </ul>
    <h2>&nbsp;</h2>
</div> 
<!-- end id=text_box -->
        </div> 
        <!-- end id=content_area -->
		
<?php include 'includes/footer.html'; ?>	

	<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4/jquery.min.js"></script>
	<script type="text/javascript" src="js/tooltips.min.js"></script>
	<link rel="stylesheet" href="blueprint-css/tooltips.min.css" />
	

    
</body>
</html>
