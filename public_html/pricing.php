<?php include 'includes/doctype.html'; ?>	

<title>Pricing for Winnipeg's Empress Luxury Liner: a rental and charter luxury bus. Special event transportation for weddings, executive meeting transport, management retreats, entertainment and music groups, golf events. Finest coach service in Manitoba.</title>
<?php include 'includes/header.html'; ?>	

            <div id="main_nav">
                            
                <div id="usual1" class="usual"> 
                                            <ul> 
                        <li><a href="features.php">Features</a></li> 

                        <li><a href="contact.php">Contact Us</a></li> 
                        <li><a href="pricing.php">Pricing</a></li>
                        <li> </li>
                      </ul> 
                    </div>  <!-- end  id="usual1" class="usual" -->

                     <div id="sub_nav_new">
                          <ul> 
                            <li> </li> 
                        </ul> 
                     </div> <!-- end id=main_nav -->
          </div> <!-- end id=header -->
        
        <body id="body_pricing">
        
        <div id="content_area_sub">
       	  <div id="main_image"><img src="images/interior_detail.jpg" alt="bus interior, coach detailing" /></div>
	    <div id="text_box_single">
			  <h1>Pricing</h1>
                              
                
<p>Pricing varies on the needs of our clients, below are the average pricing levels for different levels of service.</p>
<p><strong>SPECIAL PACKAGE PRICING</strong></p>
<p><br />
  <strong>The &quot;Day&quot;</strong><br />
  $1025/day includes<br />
  •   16 passenger luxury coach,  private driver,  pick up and drop off within Winnipeg's Perimeter Highway between 10am and 6pm<br />
  •      PAYMENT - Deposit of $400, balance due 30 days prior. <br />
  <br />
  <strong>The &quot;Weekender&quot;</strong><br />
  $3000 includes<br />
  •     16 passenger luxury coach,  private driver,  pick up and drop off   within Winnipeg's Perimeter Highway for up to a 72 hour period between   Friday to Monday,  mileage to and from destination within 300 km from   point of pick-up.<br />
  •  Not included is you driver's room at your destination and GST if applicable<br />
  •      PAYMENT - Deposit of $500, balance due 30 days prior.<br />
</p>
<p>*Payments for both packages are non-refundable within 30 days </p>
<p><strong>CUSTOM PRICING</strong><br />
  We know that one size doesn't fit all. Please contact us for customized trips and pricing! Just tell us what your needs are and we'll do our best to help.</p>
<p><strong>Special Requests for Special Occasions: </strong></p>
<p>Have something EXTRA SPECIAL planned? Tell us what you would like?</p>
<p>&nbsp;</p>
	    </div> <!-- end id=text_box -->
        </div> 
        <!-- end id=content_area -->
		
<?php include 'includes/footer.html'; ?>	

</body>
</html>
