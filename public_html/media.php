<?php include 'includes/doctype.html'; ?>	

<title>Winnipeg's Empress Luxury Liner: a rental and charter luxury bus liner. Special event transportation for weddings, executive meeting transport, management retreats, entertainment and music groups, golf events. Finest coach service in Manitoba. Media enquiries.</title>
	
<?php include 'includes/header.html'; ?>	

            <div id="main_nav">
                            
                <div id="usual1" class="usual"> 
                                            <ul> 
                        <li><a href="features.php">Features</a></li> 

                                                <li><a href="contact.asp">Contact Us</a></li> 
                        <li><a href="pricing.php">Pricing</a></li>
                        <li> </li>
                      </ul> 
                    </div>  <!-- end  id="usual1" class="usual" -->

                     <div id="sub_nav_new">
                          <ul> 
                            <li> </li> 
                        </ul> 
                     </div> <!-- end id=main_nav -->
          </div> <!-- end id=header -->
        
        <body id="body_media">
        
        <div id="content_area_sub">
        	<div id="main_image"><img src="images/sub_image.jpg" alt="bsu tours of Winnipeg are available with this luxury liner" width="1000" height="225" /></div>
<div id="text_box_single">
			  <h1>In the News</h1>
                              
                
                  <p>For media inquiries, please contact our administration offices in Winnipeg, Manitoba at:</p>
                  <p>Toll Free 1-800-987-3857<br />
                  Local 204-987-3321<br />
                  Fax 204-987-3338<br />
                  <a href="mailto:">Contact us via E-mail</a></p>
</div> 
			<!-- end id=text_box -->
        </div> 
        <!-- end id=content_area -->
		
<?php include 'includes/footer.html'; ?>	

</body>
</html>
